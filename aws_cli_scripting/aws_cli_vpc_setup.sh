#!/bin/bash


#Create VPC and Store VPC ID
vpc_id=$(aws ec2 create-vpc --cidr-block 172.16.0.0/16 --query Vpc.VpcId --output text)

#Name the VPC
aws ec2 create-tags --resources $vpc_id --tags Key=Name,Value=nasp_vpc_2

#Record VPC ID to file
echo "vpc_id=$vpc_id" >> state_file

#Create Subnet and Store the ID
subnet_id=$(aws ec2 create-subnet  --vpc-id $vpc_id --cidr-block 172.16.2.0/24 --query Subnet.SubnetId  --output text)

#Record Subnet ID to file
echo "subnet_id=$subnet_id" >> state_file

#Name the subnet
aws ec2 create-tags --resources $subnet_id --tags Key=Name,Value=nasp_sn_web_2

#Create and Attach Gateway, storing the ID
gateway_id=$(aws ec2 create-internet-gateway --query InternetGateway.InternetGatewayId --output text)

#Record gateway ID to file
echo "gateway_id=$gateway_id" >> state_file

#Set gateway Name
aws ec2 create-tags --resources $gateway_id --tags Key=Name,Value=nasp_gw_2

#Attach Internet Gateway to VPC
aws ec2 attach-internet-gateway --internet-gateway-id $gateway_id --vpc-id $vpc_id

#Create Route-Table and store ID
route_table_id=$(aws ec2 create-route-table --vpc-id $vpc_id --query RouteTable.RouteTableId --output text)

#Record Route Table ID to file
echo "route_table_id=$route_table_id" >> state_file

#Set Routing Table Name
aws ec2 create-tags  --resources $route_table_id  --tags Key=Name,Value=nasp_web_rt_2

#Associate Routing Table with Subnet and store association ID
rt_association_id=$(aws ec2 associate-route-table --route-table-id $route_table_id --subnet-id $subnet_id --query AssociationId --output text)

#Record RT association ID to file
echo "rt_association_id=$rt_association_id" >> state_file

#Add default route to routing table
aws ec2 create-route --route-table-id $route_table_id --destination 0.0.0.0/0 --gateway-id $gateway_id --output text

#Create security Group and store ID
security_group_id=$(aws ec2 create-security-group --group-name nasp_web_sg_2 --description "Allow http, https, and ssh access from bcit and home" --vpc-id $vpc_id --query GroupId --output text)

#Record Security Group ID to file
echo "secuirty_group_id=$security_group_id" >> state_file

#Create inbound rules to allow SSH from BCIT
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 22 --cidr 142.232.0.0/16

#Create inbound rule to allow HTTP from BCIT
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 80 --cidr 142.232.0.0/16

#Create inbound rule to allow HTTPS from BCIT
aws ec2 authorize-security-group-ingress --group-id $security_group_id --protocol tcp --port 443 --cidr 142.232.0.0/16
