#!/bin/bash -

#Create Elastic Ip and store allocation ID
elastic_ip_allocation_id=$(aws ec2 allocate-address  --domain vpc  --query AllocationId  --output text)

#Record to Elastic IP Allocation to file
echo "elastic_ip_allocation_id=$elastic_ip_allocation_id" >> state_file

#Discover and Record the Elastic IP address
elastic_ip=$(aws ec2 describe-addresses \
                          --allocation-ids $elastic_ip_allocation_id \
                          --query Addresses[*].PublicIp \
                          --output text)

#Record Elastic IP to file:
echo "elastic_ip=$elastic_ip" >> state_file
