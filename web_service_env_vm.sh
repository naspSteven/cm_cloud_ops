#!/bin/bash

set -o nounset
network_name=nasp_cm_co
#network_address=192.168.254.0
#cidr_bits=24
#rule_name1=ssh_rule
#rule_name2=http_rule
#rule_name3=https_rule
#rule_name4=pxe_ssh
#protocol=tcp
#global_ip=[]
#zone=public
#global_port1=50022
#global_port2=50080
#global_port3=50443
#global_port4=50222
#local_ip=192.168.254.10
#pxe_ip=192.168.254.5
#local_port1=22
#local_port2=80
#local_port3=443
vm_name=Execution_Environment
vms_folder="/home/smao/Test_VMs/"
size_in_mb=10240
ctrlr_name1=dvd
ctrl_type1=ide
ctrlr_name2=disk
ctrl_type2=sata
port_num0=0
port_num1=1
iso_file_path=/home/smao/Downloads/CentOS-7-x86_64-Minimal-1511/CentOS-7-x86_64-Minimal-1511.iso
memory_mb=1024
device_num0=0
device_num1=1
device_num2=0
group_name=/Execution
script_dir=/home/smao/nasp_cm_co




vboxmanage createvm --name $vm_name --basefolder $vms_folder --register

vboxmanage createhd --filename ${vms_folder}${vm_name}.vdi --size $size_in_mb -variant Standard

vboxmanage storagectl $vm_name --name $ctrlr_name1 --add $ctrl_type1 --bootable on
vboxmanage storagectl $vm_name --name $ctrlr_name2 --add $ctrl_type2 --bootable on

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num0 --device $device_num0 --type dvddrive --medium $iso_file_path

vboxmanage storageattach $vm_name --storagectl $ctrlr_name1 --port $port_num1 --device $device_num1 --type dvddrive --medium "/usr/share/virtualbox/VBoxGuestAdditions.iso"

vboxmanage storageattach $vm_name --storagectl $ctrlr_name2 --port $port_num0 --device $device_num2 --type hdd --medium ${vms_folder}${vm_name}.vdi --nonrotational on

vboxmanage modifyvm $vm_name\
    --ostype "RedHat_64"\
    --groups "$group_name"\
    --cpus 1\
    --hwvirtex on\
    --nestedpaging on\
    --largepages on\
    --firmware BIOS\
    --nic1 natnetwork\
    --macaddress1 020000000001\
    --nictype1 "82543GC"\
    --nat-network1 "$network_name"\
    --cableconnected1 on\
    --audio none\
    --boot1 disk\
    --boot2 net\
    --boot3 dvd\
    --boot4 none\
    --memory "$memory_mb"


vboxmanage startvm $vm_name --type gui




scp ${script_dir}/wp_ks_master.cfg pxe:/usr/share/nginx/html/wp_ks.cfg
scp -r ${script_dir}/setup/ pxe:/usr/share/nginx/html/setup/
#ssh pxe 'chmod ugo+r /usr/share/nginx/html/wp_ks.cfg'
#ssh pxe 'chmod ugo+rx /usr/share/nginx/html/setup'
ssh pxe 'chmod -R ugo+rx /usr/share/nginx/html/setup/*'



