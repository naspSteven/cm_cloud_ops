#!/bin/bash

set -o nounset
network_name=nasp_cm_co
network_address=192.168.254.0
cidr_bits=24
rule_name1=ssh_rule
rule_name2=http_rule
rule_name3=https_rule
rule_name4=pxe_ssh
protocol=tcp
global_ip=[]
zone=public
global_port1=50022
global_port2=50080
global_port3=50443
global_port4=50222
local_ip=192.168.254.10
pxe_ip=192.168.254.5
local_port1=22
local_port2=80
local_port3=443


vboxmanage natnetwork add --netname nasp_cm_co --network "192.168.254.0/24" --dhcp off


vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name1:$protocol:$global_ip:$global_port1:[$local_ip]:$local_port1"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name2:$protocol:$global_ip:$global_port2:[$local_ip]:$local_port2"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name3:$protocol:$global_ip:$global_port3:[$local_ip]:$local_port3"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "$rule_name4:$protocol:$global_ip:$global_port4:[$pxe_ip]:$local_port1"


sudo firewall-cmd --zone=$zone --add-port=$global_port1/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port2/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port3/tcp --permanent
sudo firewall-cmd --zone=$zone --add-port=$global_port4/tcp --permanent
sudo firewall-cmd --add-interface=em1 --permanent
sudo firewall-cmd --zone=public --permanent --add-service=http
sudo firewall-cmd --zone=public --permanent --add-service=https
sudo systemctl restart firewalld

#firewall-cmd --zone=$zone --list-all







