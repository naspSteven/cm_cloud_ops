#!/bin/bash -


echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install core
yum -y group install base
yum -y install epel-release
yum install nginx -y
yum install mariadb-server -y
yum install mariadb -y
yum install php -y
yum install php-mysql -y
yum install php-fpm -y

yum -y install kernel-devel kernel-headers dkms gcc gcc-c+
echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
mkdir vbox_cd
mount /dev/cdrom ./vbox_cd
./vbox_cd/VBoxLinuxAdditions.run
umount ./vbox_cd
rmdir ./vbox_cd

setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config



#Allow all wheel members to sudo all commands without a password by uncommenting line from /etc/sudoers
sed -i 's/^#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)/\1/' /etc/sudoers


#add a user
echo "Setting up Admin User"
useradd -m -G wheel,users admin
echo "admin ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir ~admin/.ssh/
cat > ~admin/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HVi7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUsYaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp18_admin
EOF
chown -R admin:admin ~admin/.ssh

systemctl start nginx
systemctl enable nginx
systemctl status nginx

systemctl start firewalld
systemctl enable firewalld
firewall-cmd --zone=public --add-port=22/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --zone=public --add-port=443/tcp --permanent
firewall-cmd --add-interface=enp0s3 --permanent
firewall-cmd --zone=public --permanent --add-service=http
firewall-cmd --zone=public --permanent --add-service=https
systemctl restart firewalld

systemctl start mariadb
mysql -e "UPDATE mysql.user SET Password = PASSWORD('nasp18') WHERE User = 'root'"
mysql -e "DROP USER ''@'localhost'"
mysql -e "DROP USER ''@'$(hostname)'"
mysql -e "DROP DATABASE test"
mysql -e "FLUSH PRIVILEGES"
systemctl enable mariadb

#PHP SETUP
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php.ini

sed -i 's/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm\/php-fpm.sock/' /etc/php-fpm.d/www.conf

sed -i 's/;listen.owner = nobody/listen.owner = nobody/' /etc/php-fpm.d/www.conf
sed -i 's/;listen.group = nobody/listen.group = nobody/' /etc/php-fpm.d/www.conf

sed -i 's/user = apache/user = nginx/' /etc/php-fpm.d/www.conf
sed -i 's/group = apache/group = nginx/' /etc/php-fpm.d/www.conf

systemctl start php-fpm
systemctl enable php-fpm


cat <<EOF> /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;
        index index.php index.html index.htm;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
        location ~ \.php$ {
            try_files \$uri =404;
            fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            include fastcgi_params;
        }
    }
}
EOF

echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php

systemctl restart nginx

touch /home/admin/wordpress_setup.sql
cat <<EOF> /home/admin/wordpress_setup.sql
    CREATE DATABASE wordpress;
    CREATE USER wordpress_user@localhost IDENTIFIED BY 'nasp18';
    GRANT ALL PRIVILEGES ON wordpress.* TO wordpress_user@localhost;
    FLUSH PRIVILEGES;
EOF

mysql -u root -pnasp18 < /home/admin/wordpress_setup.sql
mysql -u root -pnasp18 -e "SELECT user FROM mysql.user;"
mysql -u root -pnasp18 -e "SHOW DATABASES;"


wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp wordpress/wp-config-sample.php wordpress/wp-config.php

sed -i "s/database_name_here/wordpress/" ./wordpress/wp-config.php
sed -i "s/username_here/wordpress_user/" ./wordpress/wp-config.php
sed -i "s/password_here/nasp18/" ./wordpress/wp-config.php

rsync -avP wordpress/ /usr/share/nginx/html/ >/dev/null 2>&1

#mkdir /usr/share/nginx/html/wp-content/uploads  

chown -R nginx:nginx /usr/share/nginx/html/*







